@extends('layouts.app')

@section('content')
    <h1>Shopping Cart</h1>

    @if ($status)
        <p style="color:red">{{ $status }}</p>
    @endif
    <a href="{{ route('session.zbrazja') }}">Zbraze shporten</a>

    @if ($shopping_cart)
        <table class="table">
            @foreach($shopping_cart as $row)
                <tr>
                    <td>{{ $row['name'] }}</td>
                    <td>{{ $row['quantity'] }}</td>
                    <td>{{ $row['price'] }}</td>
                    <td>{{ $row['total'] }}</td>
                </tr>
            @endforeach
        </table>
    @endif
@endsection
