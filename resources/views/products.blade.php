@extends('layouts.app')

@section('content')
    <h1>Products</h1>
    <a href="{{ route('session.shopping_cart') }}">Shporta</a>
    <table class="table">
        @foreach($products as $product)
            <form method="GET" action="{{ route('session.regjistrimi') }}">
                <tr>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->price }}</td>
                    <td>
                        <input type="hidden" name="product_id" value="{{ $product->id }}">
                        <input type="number" name="quantity" value="1">
                    </td>
                    <td><input type="submit" value="ORDER"></td>

                </tr>
            </form>
        @endforeach
    </table>
@endsection
