@extends('layouts.app')

@section('content')
    <h1>Profile</h1>

    <form method="POST" action="{{ route('user.profileupdate') }}">
        @csrf
        <label for="name">Emri</label>
        <input class="form-control" type="text" value="{{ $user->name }}" name="name" id="name">
        <label for="email">Email</label>
        <input class="form-control" type="email" value="{{ $user->email }}" name="email" id="email">
        <label for="email">Password</label>
        <input class="form-control" type="password" value="" name="password" id="password">
        <input type="submit" value="RUAJE">
    </form>
@endsection

@section('sidebar')
@parent
<p>Linku 1</p>
<p>Linku 2</p>
<p>Linku 3</p>
@endsection

@section('javascript')

@endsection
