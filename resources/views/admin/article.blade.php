@extends('layouts.admin')
@section('head')
@endsection

@section('content')

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif


    <div class="row">
        <div class="col-md-8">
            <form method="POST" action="{{ route('admin.article.update') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="article_id" value="{{ $article->id }}">
                <label for="title">Titulli</label>
                <input class="form-control" type="text" id="title" name="title"
                       value="{{ old('title', $article->title) }}">
                <label for="content">Teksti</label>
                <textarea class="form-control" name="content" id="content"
                          rows="10">{{ old('content', $article->content) }}</textarea>


                <select name="category_id" id="category_id" class="form-control">
                    @foreach($categories as $category)
                        <option @if ($category->id == old('category_id', $article->category_id)) selected
                                @endif value="{{ $category->id }}">{{ $category->name }}</option>
                    @endforeach
                </select>

                <label for="image">Zgjedhe fotografine</label>
                <input type="file" name="image" id="image">


                <input type="submit" value="RUAJE" class="btn btn-primary">

            </form>
        </div>
        <div class="col-md-4">
            <div class="row" style="height: 400px; overflow: auto">
                @foreach($images as $image)
                    <div class="col-md-6">
                        <img src="{{ Storage::disk('articles')->url($image) }}" class="img-fluid">
                        <input type="text" value="{{ Storage::disk('articles')->url($image) }}" class="img-fluid browseimage">
                        <input type="text" value="<img src='{{ Storage::disk('articles')->url($image) }}' class='img-fluid' >" class="img-fluid">
                    </div>
                @endforeach

            </div>


        </div>
    </div>


@endsection

@section('js')

    <script src="https://cdn.ckeditor.com/ckeditor5/21.0.0/classic/ckeditor.js"></script>

        <script>
            ClassicEditor
                .create(document.querySelector('#content'))
                .catch(error => {
                    console.error(error);
                });
        </script>
@endsection
