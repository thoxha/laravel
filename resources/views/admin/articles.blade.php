@extends('layouts.admin')



@section('content')
    <h1>Articles</h1>
    <a href="{{ route('admin.article.new') }}" class="btn btn-primary">Shto nje artikull</a>
    <table class="table">
        @foreach($articles as $article)
            <tr>
                <td>{{ $article->id }}</td>
                <td><a href="{{ route('admin.article', ['id' => $article->id] ) }}" class="btn btn-primary btn-sm">Edit</a></td>
                <td>{{ $article->title }}</td>
                <td>{{ $article->created_at }}</td>
                <td>{{ $article->updated_at }}</td>
            </tr>

        @endforeach
    </table>
@endsection

