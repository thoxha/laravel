@extends('layouts.app')

@section('content')
    <h1>Lajmet</h1>
    <div class="row">
        @foreach($articles as $article)
            <div class="col-md-4">
                <h4><a href="{{ route('articles.show', [$article]) }}">{{ $article->title }}</a></h4>
                <div>
                    @if ($article->image)
                      <img src="/{{ $article->image }}" class="img-fluid">
                    @endif
                </div>
            </div>
        @endforeach

    </div>
@endsection

@section('sidebar')
    @parent
    <p>Linku A</p>
    <p>Linku B</p>
    <p>Linku C</p>
@endsection
