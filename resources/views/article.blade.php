@extends('layouts.app')

@section('content')
    <h1>{{ $article->title }}</h1>

    @if ($article->image)
        <img src="/{{ $article->image }}" class="img-fluid">
    @endif

    <div>{!! $article->content !!}</div>

    <p>Kategoria: {{ $article->category->name }}</p>
    <p>Autori: {{ $article->user->name }}</p>


    <div id="comment_form">
        @if (Auth::check())
            <h1>Formulari i komentit</h1>

            <div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>

            @if (Auth::check())
                <form method="POST" action="{{ route('commentSave') }}">
                    @csrf
                    <input type="hidden" name="article_id" value="{{ $article->id }}">
                    <label for="comment">Komenti juaj</label>
                    <textarea name="comment" id="comment" class="form-control" rows="10">{{ old('comment') }}</textarea>

                    <input type="submit" value="DERGO" class="btn btn-primary">
                </form>
            @endif
        @endif
    </div>

    <div id="comments">
        @foreach($article->comments as $comment)
            <p>Komentoi: {{ $comment->article->user->name }}</p>
            <p>Data: {{ date("d.m.Y H:i:s", strtotime($comment->created_at)) }}</p>
            <div>Komenti: {{ $comment->comment }}</div>
            <hr>
        @endforeach
    </div>

@endsection
