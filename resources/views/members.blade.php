@extends('layouts.app')

@section('content')
    <h1>@lang('messages.club_members')</h1>
    <ul>
   @foreach($members as $member)
            <li><a href="{{ route('member', [$member]) }}">{{ $member->name }}</a></li>
   @endforeach
    </ul>
@endsection
