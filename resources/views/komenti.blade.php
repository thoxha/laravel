@extends('layouts.app')

@section('content')
    <h1>Komenti #{{ $comment->id }}</h1>
    <blockquote>{{ $comment->comment }}</blockquote>
    <p>Autori: <a href="{{ route('member', ['id' => $comment->user->id]) }}">{{ $comment->user->name }}</a></p>
    <h5>Komente tjera nga ky autor</h5>
    <div>
        @foreach($comment->user->comments  as $comm)
            @if ($comm->id != $comment->id )
                <p><a href="{{ route('commentShow',
 ['id' => $comm->id]) }}" class="btn btn-primary">#{{ $comm->id }}</a> : {{ mb_substr($comm->comment,0, 20) }}...</p>
            @endif
        @endforeach
    </div>
@endsection
