@extends('layouts.app')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ route('files.upload') }}" enctype="multipart/form-data">
        @csrf
        <input type="text" name="emri" placeholder="Sheno emrin">

        <input type="file" name="image[]" multiple>


        <input type="submit" value="DERGO">
    </form>
@endsection
