@extends('layouts.app')

@section('content')

    @if ($tourist_data->has('tourist_name'))
        <h1>{{ $tourist_data['tourist_name'] }}</h1>
    @endif
    <form method="POST" action="{{ route('collections.request') }}">

        @csrf

        <input type="text" name="student_name" placeholder="Student name">
        <input type="text" name="school_id" placeholder="School ID">
        <input type="text" name="city_id" placeholder="City ID">
        <input type="checkbox" name="pamje_nga_deti"> Pamje nga deti
        <input type="submit" value="SUBMIT">
    </form>

@endsection
