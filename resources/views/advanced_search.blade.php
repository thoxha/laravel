@extends('layouts.app')

@section('content')
    <form method="GET" action="{{ route('articles.search') }}">
    <label for="search_field">Sheno nje fjale</label>
    <input type="text" name="search_field" id="search_field" class="form-control">

    <label for="category_id">Zgjedhe kategorine</label>
    <select name="category_id" id="category_id" class="form-control">
        <option value="0">Te gjitha</option>
        @foreach($categories as $category)
            <option value="{{ $category->id }}">{{ $category->name }}</option>
        @endforeach
    </select>

        <label for="user_id">Zgjedhe autorin</label>
        <select name="user_id" id="user_id" class="form-control">
            <option value="0">Te gjithe</option>
            @foreach($authors as $author)
                <option value="{{ $author->id }}">{{ $author->name }}</option>
            @endforeach
        </select>

    <input type="submit" class="btn btn-primary" value="KERKO">
    </form>
@endsection
