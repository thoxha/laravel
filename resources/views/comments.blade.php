@extends('layouts.app')

@section('head')
    <style>
        .komenti {
            border: 2px solid black;
            padding: 1em;
            margin-bottom: 2em;
        }
    </style>
@endsection


@section('content')
    <h1>Komentet</h1>
    @foreach($comments as $comment)
        <div class="komenti">
            <blockquote style="font-style: italic">
                {{ mb_substr($comment->comment, 0, 20) }}...

            </blockquote>
            <p> <a class="btn btn-primary" href="{{ route('commentShow', ['id' => $comment->id]) }}">Hape</a></p>
            <p>Postuesi: {{ $comment->user->name }}</p>
            <p>Emaili: {{ $comment->user->email }}</p>
            <p>Postuar me: {{ date("d.m.Y H:i:s", strtotime($comment->created_at)) }}</p>
        </div>
    @endforeach

    @if ($showPagination)
        <div>{{ $comments->links() }}</div>
    @endif
@endsection
