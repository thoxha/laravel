@extends('layouts.app')

@section('content')



    <form method="POST" action="{{ route('students.grades_save') }}">
        @csrf

        <div class="row">
            <div class="col-md-3">
                <p>Subject</p>
                <div>
                    <select name="subject_id">
                        @foreach($subjects as $subject)
                            <option value="{{ $subject->id }}">{{ $subject->subject_name }}</option>
                        @endforeach

                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <p>Student</p>
                <div>
                    <select name="student_id">
                        @foreach($students as $student)
                            <option value="{{ $student->id }}">{{ $student->student_name }}</option>
                        @endforeach

                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <select name="grade">
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
            </div>
            <div class="col-md-1">
                <select name="status">
                    <option value="1">Shtoje</option>
                    <option value="2">Fshije</option>
                </select>
            </div>
            <div class="col-md-1">
                <input type="submit" value="SUBMIT">
            </div>
        </div>


    </form>
@endsection
