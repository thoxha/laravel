@extends('layouts.app')

@section('head')
    <style>
        .komenti {
            border: 2px solid black;
            padding: 1em;
            margin-bottom: 2em;
        }
    </style>
@endsection

@section('content')
    <h1>Anetari</h1>
    <h3>{{ $member->name }}</h3>
    <p>{{ $member->email }}</p>
    <p>{{ date("d.m.Y H:i:s", strtotime($member->created_at)) }}</p>
    <h4>Komentet</h4>
    @foreach($member->comments as $comment)
        <div class="komenti">
            <blockquote style="font-style: italic">{{ $comment->comment }}</blockquote>
            <p>Postuesi: {{ $comment->user->name }}</p>
            <p>Emaili: {{ $comment->user->email }}</p>
            <p>Postuar me: {{ date("d.m.Y H:i:s", strtotime($comment->created_at)) }}</p>
        </div>
    @endforeach
@endsection
