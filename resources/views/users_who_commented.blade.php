@extends('layouts.app')

@section('content')
    @foreach($users as $user)
        <p><a href="{{ route('my_comments', ['user_id' => $user->id]) }}">{{ $user->name }}</a></p>
    @endforeach
@endsection
