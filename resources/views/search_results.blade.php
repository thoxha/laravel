@extends('layouts.app')

@section('content')
    <h1>Rezultatet e kerkimit</h1>
    <div class="row">
        @foreach($articles as $article)
            <div class="col-md-12">
                <h4><a href="{{ route('articles.show', [$article]) }}">{{ $article->title }}</a></h4>
                <blockquote>{!! mb_substr($article->content, 0, 100) !!}...</blockquote>
{{--                <div>--}}
{{--                    @if ($article->image)--}}
{{--                      <img src="/{{ $article->image }}" class="img-fluid">--}}
{{--                    @endif--}}
{{--                </div>--}}
            </div>
        @endforeach

    </div>

    <div>
        {{ $articles->appends(['search_field' => $search_field, 'category_id' => $category_id, 'user_id' => $user_id])->links() }}
    </div>
@endsection
