@extends('layouts.app')


@section('content')
    @include('partials.list')


    @{{ name }}
    <script>
        var person = @json($jsondata);
        alert(person.emri)
    </script>

    @verbatim
        <div class="container">
            Hello, {{ name }}.
            {{ age }}.
            {{ amount }}.
        </div>
    @endverbatim

    @unless (Auth::check())
        You are not signed in. Please sign in.
    @endunless

    @isset($emri)
        {{ $emri }}
    @endisset

    @empty($records)
        Nuk ka asgje ne shporten tuaj
    @endempty

    @auth
        Logout
    @endauth

    <!-- KOMENT NE HTML  -->
    {{--    @auth('admin')--}}
    {{--       Dashboard--}}
    {{--    @endauth--}}

    {{--    @guest('admin')--}}
    {{--        // The user is not authenticated...--}}
    {{--    @endguest--}}

    @guest
        Login
    @endguest




    @for ($i = 0; $i < 10; $i++)
        <p>The current value is {{ $i }}</p>
    @endfor

{{--    @foreach ($users as $user)--}}
{{--        <p>This is user {{ $user->id }}</p>--}}
{{--    @endforeach--}}

    @php
        $counter = 0
    @endphp

    @while ($counter < 5)
        <p>I'm looping forever.</p>

        @php
            $counter++
        @endphp
    @endwhile
@endsection

@section('javascript')
    <script>

            alert(1)
    </script>
@endsection
