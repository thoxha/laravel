<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class OrderConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    protected $order;


    public function __construct($order)
    {
        $this->order  = $order;
    }

    public function build()
    {
        return $this->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'))
            ->replyTo('marketing@akademiavirtuale.com', "Akademia Virtuale Marketing")
            ->subject("Konfirmim i porosise")
            ->view('emails/konfirmimi')
            ->text('emails/konfirmimi_text')
            ->attach('images/Catalog_WACA.pdf')
            ->with([
                'Amount' => $this->order['vlera'],
                'DueDate' => $this->order['afati'],
            ]);
    }
}
