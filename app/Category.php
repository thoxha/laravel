<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    public $timestamps = false;


    public function articles() {
        return $this->hasMany('App\Article');
    }

    public function comments() {
        return $this->hasManyThrough('App\Comment', 'App\Article');
    }

}
