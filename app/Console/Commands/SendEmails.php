<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'orders:sendemails {user_id} {--queue=5}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Dergon emaila klienteve qe kane kryer porosi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info("U ekzekutua");

        die();
        $users = \App\User::all();

        $bar = $this->output->createProgressBar(count($users));

        $bar->start();

        foreach ($users as $user) {
            sleep(1);
            $bar->advance();
        }

        $bar->finish();

        die();
        $headers = ['Name', 'Email'];

        $users = \App\User::all(['name', 'email'])->toArray();

        $this->table($headers, $users);

        die();
        $this->info("info");
        $this->error("error");
        $this->line("line");

        die();
        $name = $this->choice(
            'What is your name?',
            ['Taylor', 'Dayle'],
            1,
            $maxAttempts = null,
            $allowMultipleSelections = false
        );

        die();

        $email = $this->anticipate('Sheno emailin', ['tahir.hoxha@gmail.com', 'alkimisti@live.com']);
        $password = $this->secret('What is the password?');

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            if ($this->confirm('Do you wish to continue?')) {
                $this->info('Ekzekutimi');
            }

        } else {
            $this->error('Nuk je anetar');
        }


        die();
        $user = \App\User::find($id);


        if ($user != null) {
            $this->info($user->email);
        }

        die();
        foreach($this->argument('user_id') as $user_id) {
            $user = \App\User::find($user_id);
            if ($user != null) {
                $this->info($user->email);
            }
        }
        die();
        if ($this->option('queue') != null) {
            $this->comment("Veprim shtese");
            $this->comment($this->option('queue'));
        }

        if ($this->argument('user_id') == null) {
            $user_id = 1;
        } else {
            $user_id = $this->argument('user_id');
        }
       $user = \App\User::find($user_id);
       if ($user != null) {
           $this->info($user->email);
       } else {
           $this->info("Nuk e gjeta");
       }

        $this->info($this->calculate());
    }


    public function calculate() {
        return 3*7;
    }
}
