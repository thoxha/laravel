<?php

namespace App\Providers;

use Illuminate\Auth\Access\Response;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function ($user, $ability) {
            if (Auth::user()->level == 3) {
                return true;
            }
        });


        Gate::define('edit-article', function ($user, $article) {
            return $user->id === $article->user_id;
        });




        Gate::define('update-article', function ($user, $article) {
            if ((in_array(Auth::user()->email, config('defaults.admins'))
                || Auth::user()->level == 3)) {
                if ($user->id === $article->user_id) {
                    return Response::allow();
                } else {
                    Response::deny('Ju nuk jeni autor i artikullit!');
                }
            } else {
                Response::deny('Ju nuk jeni administrator!');
            }


        });



        Gate::after(function ($user, $ability, $result, $arguments) {

        });
    }
}
