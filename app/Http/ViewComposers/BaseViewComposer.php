<?php

namespace App\Http\ViewComposers;

use App\Article;
use App\User;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BaseViewComposer
{


    public function compose(View $view)
    {
        $data = $view->getData();

        $data['search_field'] = request()->input('search_field');

        $data['base_isAdmin'] = false;
        if (Auth::check()) {
            if (in_array(Auth::user()->email, config('defaults.admins'))
                || Auth::user()->level == 3) {
                $data['base_isAdmin'] = true;
            }
        }

        $data['base_articlesCount'] = Article::count();

        $data['users'] = User::orderBy('name')->get();

        app()->setLocale(session('lang', config('app.locale')));

        $view->with($data);
    }
}
