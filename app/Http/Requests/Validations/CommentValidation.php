<?php

namespace App\Http\Requests\Validations;

use App\Rules\IsValidBarcode;
use Illuminate\Foundation\Http\FormRequest;

class CommentValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->sanitize();
        return
            [
                'article_id' => 'required|exists:articles,id',
                'comment' => 'required|string|min:10|max:100',
            ];
    }

    public function messages()
    {
        return [
            'comment.required' => 'Ju lutemi, shenoni :attribute',
            'comment.min' => 'Komenti juaj duhet te kete se paku 10 shkronja',
        ];
    }

    public function attributes()
    {
        return [
            'comment' => 'komentin juaj',
        ];
    }


    public function sanitize() {
        $input = $this->all();

        $input['article_id'] = (int) $input['article_id'];
        $input['comment'] = filter_var($input['comment'], FILTER_SANITIZE_STRING);
        $this->replace($input);
    }
}
