<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $this->sanitize();

        return [
            'article_id' => "required",
            'title' => "required|string|min:10|max:255",
            'content' => "required|string:min:100",
            'category_id' => "required|exists:categories,id",
            'image' => "file|image|mimes:jpeg,png"
        ];
    }


    public function sanitize() {
        $input = $this->all();

        $input['article_id'] = (int) $input['article_id'];
        $input['title'] = filter_var($input['title'], FILTER_SANITIZE_STRING);

        $input['content'] = strip_tags($input['content'],
            "<b><strong><img><i><em><a><ul><li><ol><p><h1><h2><h3><blockquote>");

        $input['category_id'] = (int) $input['category_id'];
        $this->replace($input);
    }
}
