<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsSuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::check()
            && (in_array(Auth::user()->email, config('defaults.admins')))
            || Auth::user()->is_superadmin
        ) {
            return $next($request);
        }

        return abort(404);
    }
}
