<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    public function handle($request, Closure $next)
    {
        /*
        if (Auth::check() && Auth::user()->is_admin) {
             return $next($request);
         }
        */

        if (Auth::check()) {
            if (in_array(Auth::user()->email, config('defaults.admins'))
            || Auth::user()->level == 3) {
                return $next($request);
            }
        }

        return abort(404);
    }
}
