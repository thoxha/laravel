<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class RestController extends Controller
{
    public function index()
    {
        $url = 'https://smartshopper.tech/api/product/8004123007696';
        $response = Http::get($url);

        if ($response->status() != 200) {
          return view('report', ['text' => 'Gabim ne komunikim me REST API']);
        }

        $product = $response->json();

        return view('product', ['product' => $product]);

    }
}
