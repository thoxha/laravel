<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
return view('home');
    }


    public function cacheGet() {
        $value =  cache()->remember('users', 300, function () {
            return DB::table('users')->get();
        });

        dd($value);


        $total = cache('total');
        if (!Cache::has('total')) {
            // thirre REST API
            // rate limit
            $total = [rand(1,10), rand(1,10), rand(1,10), rand(1,10),rand(1,10), rand(1,10)];
            Cache::put('total', $total, 60);
        }

        dd($total);

    }


    public function cacheset() {
        Cache::put('total', 1000, 60); // 10 Minutes
    }

    public function setlanguage($lang) {
        if (in_array($lang, ['en', 'sq'])) {
            session(['lang' => $lang]);
        } else {
            session(['lang' => config('app.locale')]);
        }
        return redirect('/');
    }

    public function test() {
        echo request()->user()->name;
        echo Auth::user()->name;


//        echo "Tung";
//    {
//        $article = new Article();
//        $title = "Thaçi të hënën shkon në Hagë për intervistim
//";
//
//        $article->user_id = 1;
//        $article->category_id = 2;
//        $article->content = "Thaçi, në një postim në Facebook, ka shkruar se bashkëluftëtarët e tij dhe ai do të përballen me dinjitet dhe integritet me drejtësinë ndërkombëtare.";
//        $article->title = $title;
//        $article->save();



    }
}
