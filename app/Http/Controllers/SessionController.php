<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class SessionController extends Controller
{

    public function products()
    {
        $data['products'] = Product::orderBy('name')->get();
        return view('products', $data);
    }

    public function leximi()
    {
        dd(session('shopping_cart', []));

    }

    public function fshirja()
    {
        session()->forget('shopping_cart');
    }

    public function regjistrimi(Request $request)
    {
        $product_id = (int)$request->input('product_id');
        $quantity = (int)$request->input('quantity');

        $shopping_cart = session('shopping_cart');
        if ($shopping_cart != null) {
            foreach ($shopping_cart as $p_id => $p_qty) {
                if ($p_id == $product_id) {
                    $list[$p_id] = $p_qty + $quantity;
                }
            }
        }

        $qty = isset($list[$product_id]) ? $list[$product_id] : $quantity;
        $shopping_cart[$product_id] = $qty;
        session(['shopping_cart' => $shopping_cart]);

        return redirect()->route('session.products');
    }

    public function shoppingCart()
    {
        $data['status'] = session('status', "");

        $data['shopping_cart'] = [];
        if (session('shopping_cart')) {
            foreach (session('shopping_cart') as $product_id => $quantity) {
                $product = Product::find($product_id);
                if ($product != null) {
                    $data['shopping_cart'][] = [
                        'id' => $product->id,
                        'name' => $product->name,
                        'quantity' => $quantity,
                        'price' => $product->price,
                        'total' => $quantity * $product->price,
                    ];
                }
            }
        }

        return view('shopping_cart', $data);
    }

    public function zbrazja()
    {
//        session()->flush();
        session()->forget('shopping_cart');
        session()->flash('status', 'Shporta u zbraz!');
        return redirect()->route('session.shopping_cart');
    }
}
