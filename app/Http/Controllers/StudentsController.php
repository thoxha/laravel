<?php

namespace App\Http\Controllers;

use App\Student;
use App\Subject;
use Illuminate\Http\Request;

class StudentsController extends Controller
{

    public function studentForm()
    {

        return view('students/student_form');
    }

    public function studentSave(Request $request)
    {
        $name = $request->input('student_name');

        $student = new Student();
        $student->student_name = $name;
        $student->save();

        echo "Student saved!";

    }


    public function subjectForm()
    {

        return view('students/subject_form');
    }

    public function subjectSave(Request $request)
    {
        $name = $request->input('subject_name');

        $subject = new Subject();
        $subject->subject_name = $name;
        $subject->save();

        echo "Subject saved!";

    }


    public function grades()
    {
        $data['subjects'] = Subject::orderBy('subject_name')->get();
        $data['students'] = Student::orderBy('student_name')->get();

        return view('students/grades', $data);
    }

    public function gradesSave(Request $request)
    {
        $student_id = (int)$request->input('student_id');
        $subject_id = (int)$request->input('subject_id');
        $grade = (int)$request->input('grade');
        $status = $request->input('status');

        $student = Student::find($student_id);

        if ($status == 1) {
            $student->subjects()->attach($subject_id, ['grade' => $grade]);
            echo "U regjistrua nota me sukses!";
        } else if ($status == 2) {
            $student->subjects()->detach($subject_id);
            echo "U fshi nota me sukses!";
        }
    }

    public function report(Student $student) {
        echo "<h1>".$student->student_name."</h1>";

        foreach($student->subjects as $subject) {
            echo "<p>".$subject->subject_name.": ".$subject->pivot->grade."</p>";
        }
    }
}
