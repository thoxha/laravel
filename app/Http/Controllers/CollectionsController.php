<?php

namespace App\Http\Controllers;

use App\Student;
use App\User;
use Illuminate\Http\Request;

class CollectionsController extends Controller
{
    public function reject()
    {


        $array = ['taylor', 'abigail', "margarita"];
        foreach ($array as $item) {
            echo "<p>" . $item;
        }


        $collection = collect($array)->map(function ($item) {
            return substr($item, 0, 2);
        });

        foreach ($collection as $item) {
            echo "<p>" . $item;
        }


        $array = [5, 8, 3];
        $collection = collect($array)->map(function ($item) {
            return $item * $item;
        })->reject(function ($item) {
            return $item > 50;
        });

        foreach ($collection as $item) {
            echo "<p>" . $item;
        }

        $students = Student::all();
        $students = $students->map(function ($item) {
            return strtoupper($item);
        });

        foreach ($students as $student) {
            $st = json_decode($student, true);
            echo "<p>" . $st['STUDENT_NAME'];
        }

        $average = collect([1, 1, 2, 4])->avg();
        echo "<p>" . $average . "</p>";


        $collection = collect([1, 2, 3, 4, 5, 6, 7]);

        $chunks = $collection->chunk(4);
        foreach ($chunks as $chunk) {
            foreach ($chunk as $item) {
                echo $item . " ";
            }
        }


        $collection = collect([[1, 2, 3], [4, 5, 6], [7, 8, 9]]);
        $collapsed = $collection->collapse();
        // [1, 2, 3, 4, 5, 6, 7, 8, 9]


        $collection = collect([1, 2, 2, 2, 3, 5, 5, 2, 1, 4, 2]);

        $counted = $collection->countBy();
        foreach ($counted->all() as $nota => $numri) {
            echo "<p>" . $nota . " : " . $numri . "</p>";

        }


        $collection = collect(['white', 'black', 'red', 'blue']);

        $matrix = $collection->crossJoin(['S', 'M', "L", "XL", "XXL"]);

        echo "<select name='selection'>";
        foreach ($matrix->all() as $item) {
            echo "<option value='" . $item[0] . "-" . $item[1] . "'>" . $item[0] . " - " . $item[1] . "</option>";

        }
        echo "</select>";

        $collection = collect([6, 3, 3, 4, 6]);

        if ($collection->count() > 0) {
            $success = $collection->every(function ($value) {
                return $value > 2;
            });

            if ($success) {
                echo "<p>Te gjithe anetaret e plotesojne kushtin!";
            } else {
                echo "<p>Ka anetare qe nuk e plotesojne kushtin";
            }
        } else {
            echo "<p>Nuk ka asnje anetar!";
        }


        $collection = collect([1, 2, 3, 4]);

        $filtered = $collection->filter(function ($value, $key) {
            $user = User::find($value);
            if ($value > 2 && $user != null) {
                return true;
            } else {
                return false;
            }
        });


        $collection = collect([
            ['account_id' => 'account-x10', 'product' => 'Chair'],
            ['account_id' => 'account-x10', 'product' => 'Bookcase'],
            ['account_id' => 'account-x11', 'product' => 'Desk'],
        ]);

        $grouped = $collection->groupBy('account_id');

        $collection = collect([2, 4, 6, 8]);

        $index = $collection->search(2);

        if ($index === false) {
            echo "Nuk e gjeta";
        } else {
            echo "E gjeta";
        }

// 1

    }


    public function requestform()
    {
        $tourist_data = collect(
            [
                'tourist_name' => "Adnan",
                'payment' => 123
            ]
        );
        return view('requestform', compact('tourist_data'));
    }

    public function requestcollection(Request $request)
    {
        if ($request->has('pamje_nga_deti')) {
            echo "Pamje nga deti";

        } else {
            echo "Nuk ka pamje nga deti";
        }
        $input = $request->only('_token', 'student_name');

    }


}
