<?php

namespace App\Http\Controllers;

use App\User;
use Facade\Ignition\Support\Packagist\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class JsonController extends Controller
{
    //

    public function index()
    {
        $userData = [
            'language' => "en",
            'country' => "al"

        ];

        $jsonData = json_encode($userData);

        $user = User::find(1);
        $user->options = $jsonData;
        $user->save();

    }

    public function jsonUser($id)
    {
        $user = User::find($id);
        if ($user != null) {
            $options = json_decode($user->options, true);
            dd($options);
        }
    }

    public function jsonSearch()
    {
        $users = DB::table('users')
            ->where('options->language', 'en')
            ->get();

        dd($users);

        $users = User::where('options->language', 'en')->get();
//        dd($users);
    }


    public function form()
    {
        return view('form');
    }

    public function save(Request $request)
    {
        $user_name = $request->input('user_name');

        $users = DB::table('users')
            ->when($user_name, function ($query, $user_name) {
                return $query->where('name', 'like', '%' . $user_name . '%');
            })->get();

        if ($users->isEmpty()) {
            echo "Nuk ka asnje!";
        } else {
            dd($users);
        }

    }


    public function insertform()
    {
        return view('insertform');
    }

    public function insertformsubmit(Request $request)
    {



        $sortBy = "a"; // 0, NULL, [], ""

        $product_name = "Pop kek";
        $section_id = 7;
        $brand_id = 0;
        $sortOrder = "x";



        $users = DB::table('products')

            ->when($product_name, function ($query, $product_name) {
                return $query->where('name', "like", "%".$product_name."%");
            })

            ->when($section_id, function ($query, $section_id) {
                return $query->where('section_id', $section_id);
            })
            ->when($brand_id, function ($query, $brand_id) {
                return $query->where('brand_id', $brand_id);
            })

            ->when($sortOrder == 'a', function ($query, $sortOrder) {
                return $query->orderBy('name', 'asc');
            }, function ($query) {
                return $query->orderBy('name', 'desc');
            })

            ->dd();





        exit();

        $amount = 100;
        DB::transaction(function ($amount) {
            DB::table('users')->where('id', 2)->decrement(['balance' => $amount]);
            DB::table('bankaccounts')->where('id', 1)->increment(['balance' => $amount*0.01]);
            DB::table('users')->where('id', 111)->increment(['balance' => $amount*0.99]);

        }, 5);


        DB::beginTransaction();
        try {
            $affected = DB::table('users')->where('id', 2)->decrement(['balance' => $amount]);
            if ($affected == 0) {
                DB::rollBack();
            } else {
                $affected = DB::table('bankaccounts')->where('id', 1)->increment(['balance' => $amount * 0.01]);
                if ($affected == 0) {
                    DB::rollBack();
                } else {
                    $affected = DB::table('users')->where('id', 111)->increment(['balance' => $amount * 0.99]);
                    if ($affected == 0) {
                        DB::rollBack();
                    } else {
                        DB::commit();
                    }
                }

            }
        } catch (\Exception $e) {
            DB::rollBack();

            Log::error($e->getMessage());


            $data['report'] = "Ndodhi nje gabim!";
            return view('report', $data);
        }




//        DB::insert('insert into users (id, name) values (?, ?)', [1, 'Dayle']);
//        DB::insert('insert into users (id, name) values (:xx, :yy)', ['yy' => 'Dayle', 'xx' => 1]);


        $votes = DB::table('tests')->where('votes', '>', 5)->get();


//        $affected = DB::table('users')
//            ->where('id', 1)
//            ->update(['options->language' => 'sq']);

//        die();
//        $email = filter_var($request->input('email'), FILTER_SANITIZE_STRING);
//        $votes = (int)$request->input('votes');
//
//        $affected = DB::table('tests')
//            ->where('id', '>', 5)
//            ->update(['votes' => 1]);
//
//        echo $affected;




//
//        $members = [
//            ['email' => "a@b.com", 'votes' => 55],
//            ['email' => "a@c.com", 'votes' => 33],
//            ['email' => "a@d.com", 'votes' => 11],
//            ['email' => "a@e.com", 'votes' => 22],
//            ['email' => "a@f.com", 'votes' => 5]
//        ];
//
//
//        DB::table('tests')->insert($members);

//        $user = new User();
//        $user->email = $email;
//        $user->votes = $votes;
//        $user->save();
//
//        $id = $user->id;


//        $id = 4;
//        $user = User::find($id);
//        if ($user != null) {
//            $user->delete();
//        } else {
//            echo "Nuk ekziston!";
//        }
//
//        DB::table('users')->where('id',3)->delete();
    }

    public function bladejson() {
        $data['emri'] = "Laravel";
        $data['jsondata'] = ['emri' => "Adnan", 'profesioni' => "developer"];
        return view('json', $data);
    }

    public function send($user_id) {
        $exitCode = Artisan::call('orders:sendemails', [
            'user_id' => $user_id, '--queue' => 'default'
        ]);

    }
}
