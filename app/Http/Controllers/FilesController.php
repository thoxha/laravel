<?php

namespace App\Http\Controllers;

use App\Article;
use App\Mail\OrderConfirmation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class FilesController extends Controller
{
    public function put()
    {
        Storage::put('test.txt', "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
    }

    public function resource()
    {
        $resource = fopen('C:\laragon\www\icklaravel\README.md', 'r');
        Storage::disk('public')->put('prova.txt', $resource);
        Storage::disk('public')->prepend('prova.txt', "Prepended text");
        Storage::disk('public')->append('prova.txt', "Appended text");
        Storage::disk('public')->append('prova.txt', "Appended text");
        Storage::disk('public')->append('prova.txt', "Appended text");
        Storage::disk('public')->append('prova.txt', "Appended text");
        Storage::disk('public')->append('prova.txt', "Appended text");
        Storage::disk('public')->append('prova.txt', "Appended text");
        Storage::disk('public')->append('prova.txt', "Appended text");
    }

    public function export()
    {
        $articles = Article::all();
        Storage::disk('public')->put('export.csv', "");
        foreach ($articles as $article) {
            $row = $article->id . ", " . $article->title . ", " . $article->slug;
            Storage::disk('public')->append('export.csv', $row);
        }

        echo "Fajlli u krijua!";
    }

    public function copy()
    {
        Storage::copy('public/export.csv', 'upload/export2.csv');
        Storage::disk('products')->put('exports/export3.csv', Storage::disk('public')->get('export.csv'));
    }

    public function ftp()
    {
        Storage::disk('ftp')->put('export3.csv', Storage::disk('public')->get('export.csv'));
    }


    public function download()
    {

        return Storage::download('public/export.csv', 'prova.csv');


    }

    public function url()
    {
        $url = Storage::disk('public')->url('export.csv');
        return view('url', compact('url'));
    }

    public function directory()
    {
        $dirs = Storage::allDirectories();
        foreach ($dirs as $dir) {
            echo "<p>" . $dir . "</p>";
        }
    }

    public function files()
    {
        $files = Storage::disk('public')->allFiles();
        foreach ($files as $file) {
            $url = Storage::disk('public')->url($file);
            echo "<p><a href='" . $url . "'>" . $file . "</a></p>";
        }
    }

    public function deleteDir()
    {
        Storage::disk('products')->deleteDirectory('exports');
    }

    public function uploadForm()
    {
        return view('uploadform');
    }

    public function upload(Request $request)
    {

        $request->validate(['image.*' => "required|file|image|mimes:jpeg,png"]);

        foreach ($request->file('image') as $image) {
            $name = $image->getClientOriginalName();
            $image->storeAs('products', $name);
        }
    }

    public function browse()
    {
        $images = Storage::disk('products')->files();
        return view('browse', compact('images'));
    }

    public function getSelectedImages(Request $request)
    {
        $article_id = $request->input('article_id');

        $article = Article::find($article_id);
        $old_list = json_decode($article->images, true);

        $list = $request->input('img') == null ? [] : $request->input('img');

        foreach ($list as $key => $item) {
            if (in_array($item, $old_list)) {
                unset($list[$key]);
            }
        }
        $result = array_merge($old_list, $list);

        $deletelist = $request->input('delimg') == null ? [] : $request->input('delimg');
        foreach ($deletelist as $delitem) {
            $r = array_search($delitem, $result);
            if ($r !== false) {
                unset($result[$r]);
            }
        }
        $article->images = json_encode($result);
        $article->save();
    }


    public function send()
    {
        $data['emri'] = Auth::user()->name;
        $data['vlera'] = 123;
        $data['afati'] = "31.08.2020";

        Mail::send(['html' => 'emails/konfirmimi'], $data, function ($message) {
            $message->to(Auth::user()->email, Auth::user()->name)
                ->subject("Konfirmimi i porosise")
                ->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));

        });

        echo "Emaili u dergua me sukses";


    }

    function sendWithClass()
    {
        $data['emri'] = Auth::user()->name;
        $data['vlera'] = 123;
        $data['afati'] = "31.08.2020";
        $data['cycle'] = false;

        Mail::to(Auth::user()->email, Auth::user()->name)->send(new OrderConfirmation($data));
        echo "Emaili u dergua me sukses";

    }


}
