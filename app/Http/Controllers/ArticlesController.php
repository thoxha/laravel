<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\User;
use Illuminate\Http\Request;

class ArticlesController extends Controller
{
    public function index()
    {
        $articles = Article::orderBy('created_at', 'desc')->paginate(10);

        return view('articles', ['articles' => $articles]);

    }


    public function show(Article $article)
    {
        $images = json_decode($article->images, true);
        return view('article', ['article' => $article, 'images' => $images]);
    }

    public function paRouteModelBinding($slug)
    {
        $article = Article::where('slug', '=', $slug)->first();
        if ($article == null) {
            abort(404);
        } else {
            return view('article', ['article' => $article]);
        }

    }

    public function search(Request $request)
    {
        $data['search_field'] = filter_var($request->input('search_field'), FILTER_SANITIZE_STRING);
        $category_id = $data['category_id'] = (int)$request->input('category_id');
        $user_id = $data['user_id'] = (int)$request->input('user_id');
        $data['articles'] = Article::
        when($data['category_id'], function ($query, $category_id) {
            return $query->where('category_id', $category_id);
        })
            ->when($data['user_id'], function ($query, $user_id) {
                return $query->where('user_id', $user_id);
            })
            ->where(function ($query)  {
                $search = filter_var(request()->input('search_field'), FILTER_SANITIZE_STRING);

                $query->where('title', 'like', "%" . $search . "%")
                    ->orWhere('content', 'like', "%" . $search . "%");
            })
            ->orderBy('created_at', 'desc')
            ->paginate(3);

        return view('search_results', $data);
    }

    public function advancedSearch()
    {
        $data['categories'] = Category::orderBy('name')->get();
        $data['authors'] = User::orderBy('name')->get();
        return view('advanced_search', $data);
    }
}
