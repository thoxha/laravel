<?php

namespace App\Http\Controllers\Admin;

use App\Article;
use App\Category;
use App\Http\Requests\ArticleRequest;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('is_admin');
    }


    public function index()
    {
        return view('admin/index');
    }

    public function articles()
    {
        $articles = Article::orderBy('created_at', 'desc')->paginate(20);
        return view('admin/articles', compact('articles'));

    }

    public function newArticle()
    {
        $data['article'] = (object)[
            'id' => 0,
            'title' => "",
            'content' => "",
            'category_id' => 0,
            'user_id' => 0,
            'image' => NULL,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),
        ];

        $data['categories'] = Category::orderBy('name')->get();
        $data['images'] = Storage::disk('articles')->files();

        return view('admin/article', $data);
    }


    public function article($id = 0)
    {
        $article = Article::find($id);
        if ($article != null) {
            if (Gate::allows('edit-article', $article)) {
                $data['article'] = $article;
                $data['categories'] = Category::orderBy('name')->get();

                $data['images'] = Storage::disk('articles')->files();
                return view('admin/article', $data);
            } else {
                return view('report', ['text' => "Ju nuk jeni te autorizuar!"]);
            }
        } else {
            return view('report', ['text' => "Ky artikull nuk ekziston"]);
        }
    }

    public function update(ArticleRequest $request)
    {
        $article_id = $request->input('article_id');
        $title = $request->input('title');
        $content = $request->input('content');
        $category_id = $request->input('category_id');

        if ($article_id == 0) {
            $article = new Article;
        } else {
            $article = Article::find($article_id);
            Gate::authorize('edit-article', $article);
        }


        $article->title = $title;
        $article->content = $content;
        $article->category_id = $category_id;
        $article->user_id = Auth::id();

        if ($request->file('image') != null) {
            $file = $request->file('image');
            $file_extension = $file->getClientOriginalExtension();
            $file_full_name = $file->getClientOriginalName();
            $file_name = substr($file_full_name, 0, strlen($file_full_name) - strlen($file_extension) - 1);
            $new_file_name = $file_name . "-" . time() . "." . $file_extension;
            $filename = $file->storeAs('articles', $new_file_name);
            $article->image = $filename;
        }

        if ($article->save()) {
            return view('report', ['text' => "Artikulli u perditesua me sukses!"]);
        } else {
            return view('report', ['text' => "Artikulli NUK u perditesua!"]);
        }
    }


    public function delete($id = 0)
    {
        $article = Article::find($id);
        if ($article != null) {
            if (Gate::allows('edit-article', $article)) {
                $article->delete();
            }
        } else {
            return view('report', ['text' => "Ky artikull nuk ekziston"]);
        }
    }
}
