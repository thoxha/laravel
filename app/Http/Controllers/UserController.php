<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {


        $data['members'] = User::all();
        return view('members', $data);
    }

    public function member($id)
    {
        $member = User::find($id);
        if ($member != null) {
            return view('member', ['member' => $member]);
        } else {
            return view('report', ['text' => "Nuk ekziston ky anetar"]);
        }

    }

    public function asUser($id)
    {

        Auth::loginUsingId($id);
//        $user = User::find($id);
//
//        Auth::login($user, true);
        return redirect("/home");
    }


    public function logoutFromDevices(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            Auth::logoutOtherDevices($password);
            return redirect('/home');
        }

        if (Auth::attempt(['email' => Auth::user()->email, 'password' => $password])) {

        }

    }


    public function profile()
    {
        $data['user'] = User::find(Auth::id());
        return view('profile', $data);
    }

    public function profileUpdate(Request $request)
    {
        $name = $request->input('name');
        $email = $request->input('email');
        $password = $request->input('password');

        $user = User::find(Auth::id());
        $user->name = $name;
        $user->email = $email;
        if ($password) {
            $user->password = Hash::make($password);
        }
        $user->save();
        return view('report', ['text' => "Profili juaj u perditesua"]);
    }
}
