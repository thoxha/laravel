<?php

namespace App\Http\Controllers;

use App\Article;
use App\Comment;
use App\Http\Requests\Validations\CommentValidation;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CommentsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    // Te gjitha komentet e te gjithe anetareve
    public function all()
    {
        $comments = Comment::all();
        if (count($comments) > 0) {
            return view('comments', ['comments' => $comments, 'showPagination' => false]);
        } else {
            return view('report', ['text' => "Nuk ka ende asnje koment"]);
        }
    }

    // Te gjitha komentet e te gjithe anetareve
    public function paginate()
    {
        $comments = Comment::orderBy('created_at', 'desc')->paginate(5);
        if (count($comments) > 0) {
            return view('comments', ['comments' => $comments, 'showPagination' => true]);
        } else {
            return view('report', ['text' => "Nuk ka ende asnje koment"]);
        }
    }

    // Te gjitha komentet e nje anetari
    public function index($user_id)
    {
        $comments = Comment::where('user_id', $user_id)->orderBy('created_at', 'desc')->get();
        if (count($comments) > 0) {
            return view('comments', ['comments' => $comments, 'showPagination' => false]);
        } else {
            return view('report', ['text' => "Nuk ekziston anetari me ID: " . $user_id]);
        }
    }

    public function show($id)
    {
        $x = Comment::find($id);
        if ($x != null) {
            return view('komenti', ['comment' => $x]);
        } else {
            return view('report', ['text' => "Nuk ekziston komenti me kete ID!"]);
        }
    }


    public function commentSave(CommentValidation $request)
    {
        $comment = $request->input('comment');
        $article_id = $request->input('article_id');

        $c = new Comment();
        $c->comment = $comment;
        $c->article_id = $article_id;
        $c->user_id = Auth::id();
        if ($c->save()) {
            return view('report', ['text' => "Ju faleminderit qe komentuat!"]);
        } else {
            return view('report', ['text' => "Ndodhi nje gabim!"]);


        }
    }

    public function commentUpdate($id)
    {

        // C - create (insert)
        $c = new Comment();
        $c->comment = $comment;
        $c->article_id = $article_id;
        $c->user_id = Auth::id();
        $c->save();


        // U - update (update)
        $comment = Comment::find($id);
        $comment->comment = "sfdfsdfdsfsdf";
        $comment->article_id = 123;
        $comment->user_id = Auth::id();
        $comment->save();


        // D - delete (delete)
        $comment = Comment::find($id);
        $comment->delete();


        // R read/retrieve (select)
        $comment = Comment::find($id);
        echo $comment->created_at;
    }


    public function qb()
    {
        $users = User::where('id', '>=', 2)->get();


        $users = DB::table('users')
            ->where('id', 4)
            ->first();

        dd($users);
//
//        foreach ($users as $user) {
//            $admin = ($user->level == 3) ? "Po" : "Jo";
//            echo "<p>" . $user->id . " " . $admin . "</p>";
//        }
//        $users = DB::table('users')->get();
//        dd($users);


        $user = DB::table('users')->where('id', 3)->first();


        if ($user != null) {
//            ka te dhena
        } else {
//            nuk ka te dhena
        }
    }

    public function user($id) {
        $user = User::findOrFail($id);
        dd($user);
    }

    public function stat() {
        $users = DB::table('users')->count();
        $level = DB::table('users')->max('level');
        echo "<p>Total users:".$users;
        echo "<p>Max level:".$level;
    }


    public function product() {
        $products = DB::table('products')
            ->select('products.id as product_id', 'products.name as product_name', 'barcode',
            'brands.name as brand_name')
            ->leftJoin('product_translations', 'products.id', '=', 'product_translations.product_id')
            ->rightJoin('brands', 'brands.id', '=', 'products.brand_id')
            ->join('subcategories', 'subcategories.id', '=', 'products.subcategory_id')
            ->join('categories', 'categories.id', '=', 'subcategories.category_id')
            ->join('maincategories', 'maincategories.id', '=', 'categories.maincategory_id')
            ->where('product_translations.locale', app()->getLocale())->paginate(10);

        foreach($products as $product) {
            echo $product->product_id;
            echo $product->product_name;
            echo $product->brand_name;
            echo $product->barcode;

        }
    }


    public function myComments() {
        $data['comments'] = Comment::where('user_id', Auth::id())->orderBy('created_at', 'desc')->get();
        return view('my_comments', $data);
    }

    public function usersWhoHaveCommented() {
        $data['users'] = User::orderBy('name')->get();
        return view('users_who_commented', $data);
    }

    public function showCommentsByUser($user_id) {
        $data['comments'] = Comment::where('user_id',$user_id)->orderBy('created_at', 'desc')->get();
        return view('my_comments', $data);
    }
}
