<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail as MustVerifyEmailContract;
use Illuminate\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use MustVerifyEmail, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'city', 'alias'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'level'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'banned' => 'boolean',
    ];


    public function getAdminAttribute() {
        return $this->attributes['level'] == 3;

    }


    public function getIsAdminAttribute() {
        return Auth::user()->level == 3;

    }

    public function getIsSuperadminAttribute() {
        return Auth::user()->level == 4;

    }

    public function comments() {
        return $this->hasMany('App\Comment');
    }

    public function articles() {
        return $this->hasMany('App\Article');
    }
}
