<?php

use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Request as R;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;



Auth::routes(['verify' => true]);


Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');


Route::get('setlanguage/{lang}', 'HomeController@setlanguage')->name('setlanguage');


Route::get('/send', 'FilesController@send')->name('send');
Route::get('/send_with_class', 'FilesController@sendWithClass')->name('send_with_class');
Route::get('/demo', function() {
    $data['emri'] = Auth::user()->name;
    $data['vlera'] = 123;
    $data['afati'] = "31.08.2020";

    return new \App\Mail\OrderConfirmation($data);
});

Route::get('users_who_commented', 'CommentsController@usersWhoHaveCommented')
    ->name('users_who_commented')->middleware('auth');

Route::get('my_comments/{user_id}', 'CommentsController@showCommentsByUser')
    ->name('my_comments')->middleware('auth');

Route::get('regjistrimi', 'SessionController@regjistrimi')->name('session.regjistrimi');
Route::get('leximi', 'SessionController@leximi')->name('session.leximi');
Route::get('fshirja', 'SessionController@fshirja')->name('session.fshirja');
Route::get('zbrazja', 'SessionController@zbrazja')->name('session.zbrazja');
Route::get('produktet', 'SessionController@products')->name('session.products');
Route::get('shopping_cart', 'SessionController@shoppingCart')->name('session.shopping_cart');

Route::get('bladejson', 'JsonController@bladejson');


Route::get('profile', 'UserController@profile')->name('user.profile');
Route::post('profileupdate', 'UserController@profileUpdate')->name('user.profileupdate');


Route::get('cacheget', 'HomeController@cacheGet')->name('cacheget');
Route::get('cacheset', 'HomeController@cacheSet')->name('cacheset');


Route::get('rest_index', 'RestController@index')->name('rest.index');


Route::get('/asuser/{id}', 'UserController@asUser')->name('asUser');


Route::get('/test', 'HomeController@test')->name('test');
Route::get('/json', 'JsonController@index')->name('json');
Route::get('/jsonuser/{id}', 'JsonController@jsonUser')->name('jsonuser');
Route::get('/jsonsearch', 'JsonController@jsonSearch')->name('jsonsearch');
Route::get('/form', 'JsonController@form')->name('form');
Route::post('/save', 'JsonController@save')->name('form.save');


Route::get('/insertform', 'JsonController@insertform')->name('inserform');
Route::post('/insertformsubmit', 'JsonController@insertformsubmit')->name('insertformsubmit');


Route::prefix('members')->middleware('auth')->group(function () {
    Route::get('/', 'UserController@index')->name('members');
    Route::get('/{id}', 'UserController@member')->name('member');
});


Route::prefix('comment')->group(function () {
    Route::get('/all', 'CommentsController@all')->name('commentAll');
    Route::get('/paginate', 'CommentsController@paginate')->name('commentPaginate');
    Route::get('/index/{user_id}', 'CommentsController@index')->name('commentIndex');
    Route::get('/show/{id}', 'CommentsController@show')->name('commentShow');
    Route::get('/form', 'CommentsController@commentForm')->name('commentForm');
    Route::post('/save', 'CommentsController@commentSave')->name('commentSave');
});


Route::prefix('articles')->group(function () {
    Route::get('/index', 'ArticlesController@index')->name('articles.index');
    Route::get('/search', 'ArticlesController@search')->name('articles.search');
    Route::get('/advanced_search', 'ArticlesController@advancedSearch')->name('articles.advanced_search');
    Route::get('/show/{article}', 'ArticlesController@show')->name('articles.show');
});



Route::prefix('admin')->middleware('is_admin')->group(function () {
    Route::get('/index', 'Admin\AdminController@index')->name('admin.index');
    Route::get('/articles', 'Admin\AdminController@articles')->name('admin.articles');
    Route::get('/article/{id}', 'Admin\AdminController@article')->name('admin.article');
    Route::get('/article/new', 'Admin\AdminController@newArticle')->name('admin.article.new');
    Route::post('/update', 'Admin\AdminController@update')->name('admin.article.update');
});


Route::get('/qb', 'CommentsController@qb')->name('qb');
Route::get('/user/{id}', 'CommentsController@user')->name('user');
Route::get('/stat', 'CommentsController@stat')->name('stat');



Route::prefix('students')->group(function () {
    Route::get('student_form', 'StudentsController@studentForm')->name('students.student_form');
    Route::post('student_save', 'StudentsController@studentSave')->name('students.student_save');
    Route::get('subject_form', 'StudentsController@subjectForm')->name('students.subject_form');
    Route::post('subject_save', 'StudentsController@subjectSave')->name('students.subject_save');
    Route::get('grades', 'StudentsController@grades')->name('students.grades');
    Route::post('grades_save', 'StudentsController@gradesSave')->name('students.grades_save');
    Route::get('report/{student}', 'StudentsController@report')->name('students.report');
});

Route::prefix('collections')->group(function() {
   Route::get('reject', 'CollectionsController@reject')->name('collections.reject');
   Route::get('requestform', 'CollectionsController@requestform')->name('collections.requestform');
   Route::post('request', 'CollectionsController@requestcollection')->name('collections.request');
});

Route::prefix('files')->group(function() {
    Route::get('put', 'FilesController@put')->name('files.put');
    Route::get('resource', 'FilesController@resource')->name('files.resource');
    Route::get('export', 'FilesController@export')->name('files.export');
    Route::get('copy', 'FilesController@copy')->name('files.copy');
    Route::get('ftp', 'FilesController@ftp')->name('files.ftp');
    Route::get('download', 'FilesController@download')->name('files.download');
    Route::get('url', 'FilesController@url')->name('files.url');
    Route::get('directory', 'FilesController@directory')->name('files.directory');
    Route::get('files', 'FilesController@files')->name('files.files');
    Route::get('delete_dir', 'FilesController@deleteDir')->name('files.delete_dir');
    Route::get('uploadform', 'FilesController@uploadForm')->name('files.uploadform');
    Route::post('upload', 'FilesController@upload')->name('files.upload');
    Route::get('browse', 'FilesController@browse')->name('files.browse');
    Route::get('get_selected_images', 'FilesController@getSelectedImages')->name('files.get_selected_images');
});




Route::get('/dergoemailat/{user_id}', 'JsonController@send')->name('dergoemailat');




/*
// Authentication Routes...
Route::get('{lang}/login', [
    'as' => 'login',
    'uses' => 'Auth\LoginController@showLoginForm'
]);

Route::post('{lang}/login', [
    'as' => '',
    'uses' => 'Auth\LoginController@login'
]);
Route::any('{lang}/logout', [
    'as' => 'logout',
    'uses' => 'Auth\LoginController@logout'
]);

// Password Reset Routes...
Route::post('{lang}/password/email', [
    'as' => 'password.email',
    'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
]);
Route::get('{lang}/password/reset', [
    'as' => 'password.request',
    'uses' => 'Auth\ForgotPasswordController@showLinkRequestForm'
]);
Route::post('{lang}/password/reset', [
    'as' => 'password.update',
    'uses' => 'Auth\ResetPasswordController@reset'
]);
Route::get('{lang}/password/reset/{token}', [
    'as' => 'password.reset',
    'uses' => 'Auth\ResetPasswordController@showResetForm'
]);

// Registration Routes...
Route::get('{lang}/register', [
    'as' => 'register',
    'uses' => 'Auth\RegisterController@showRegistrationForm'
]);
Route::post('{lang}/register', [
    'as' => '',
    'uses' => 'Auth\RegisterController@register'
]);
*/
